#! /bin/bash
CURDIR=`dirname $0`
. ${CURDIR}/config

RESULTPRINTER="${RCLM_PREFIX}/bin/rclmresultprinter.3d"
RESULTFILE=./var/results

TIE_BREAK_SETTING=$3
if [ "$TIE_BREAK_SETTING" = "tiebreak" ]; then
    FULL_TIME=$TIE_BREAK_GAME_TIME
else
    FULL_TIME=$TOTAL_GAME_TIME
fi
HALF_TIME=$((FULL_TIME/2))

DELIMITER_PLAYERS="|"
# Some valid team name examples: myteam, /home/myteam, /home/myteam:myteam
TEAM1=$1
TEAM2=$2
LOCAL_HOST='127.0.0.1'

PARAMETERS=("$@")
TEAMALL1=()
TEAMALL2=()
isFOUND_DELIMITER=false
for ((i=3; i<=${#PARAMETERS[@]}; i++))
do
	if [ "${PARAMETERS[${i}]}" = "|" ] ; then
		isFOUND_DELIMITER=true
	elif $isFOUND_DELIMITER ; then
		TEAMALL2+=(${PARAMETERS[${i}]})
	else
		TEAMALL1+=(${PARAMETERS[${i}]})
	fi
done

echo "$TEAM1 vs $TEAM2"

NAME1=`basename "${TEAM1#*:}"`
NAME2=`basename "${TEAM2#*:}"`
DIR1=${TEAM1%:*}
DIR2=${TEAM2%:*}


ssh ${SERVER} "killall -9 rcssserver3d  &> /dev/null"

echo Running RCSSServer3D
ssh ${SERVER} "sed -i 's/\(addSoccerVar(.RuleHalfTime.\).*)/\1, ${HALF_TIME})/' $SOCCERSIM_CONF"
ssh ${SERVER} "${SERVER_PREFIX_DIR}/bin/rcssserver3d &> rcssserver3d.log" &
SERVER_PID=$!
sleep 2
if [ "$USED_MONITOR" = "rcssmonitor3d" ]; then
    echo Running External 3D Monitor
    rcssmonitor3d --server $SERVER &> rcssmonitor3d.log &
    MONITOR_PID=$!
    sleep 2
elif [ "$USED_MONITOR" = "roboviz" ]; then
    if pgrep -lf RoboViz &> /dev/null; then
        echo "RoboViz is (apparently) already running"
        MONITOR_PID=
    else
        echo Running RoboViz
        pushd ~/Desktop/roboviz/
        ./roboviz.sh &> roboviz.log &
	popd
        MONITOR_PID=$!
        sleep 4
    fi
else
    MONITOR_PID=$SERVER_PID
fi

echo Running Team $NAME1 on $LOCAL_HOST
N=2
for REAL_NAME in ${TEAMALL1[@]}
do
	echo $REAL_NAME $N
	ssh ${CLIENT1} "su - $REAL_NAME -c './start_dropin.sh $LOCAL_HOST -t $NAME1 -u $N &> log/start.log'" &
	N=`expr $N + 1`
	sleep 1
done
	
sleep 10

echo Running Team $NAME2 on $LOCAL_HOST
N=2
for REAL_NAME in ${TEAMALL2[@]}
do
	echo $REAL_NAME $N
	ssh ${CLIENT2} "su - $REAL_NAME -c './start_dropin.sh $LOCAL_HOST -t $NAME2 -u $N &> log/start.log'" &
	N=`expr $N + 1`
	sleep 1
done

if [ "$RESTORE" = true ]; then
	echo -n "Restore the match? [y/N]:"
    	read restore
    	if [ $restore = "y" -o $restore = "Y" ]; then
    		ssh ${SERVER} "./restore.py ./log_backup/sparkmonitor.log 127.0.0.1 3200"
    	fi
	sed -i 's/\RESTORE=true/\RESTORE=false/' ./var/conf
fi

if [ "$USED_MONITOR" = "roboviz" ]; then
    while [ "$TERMINATE" != "end" ]; do
        read -p "Type 'end' when game is finished or to terminate the game: " TERMINATE
    done
else
    wait $MONITOR_PID
fi

# Finished
echo Killing the simulator
ssh ${SERVER} "
killall -s INT rcssserver3d
sleep 2
killall -9 rcssserver3d &> /dev/null
"

echo Killing Team $NAME1
for REAL_NAME in ${TEAMALL1[@]}
do
	ssh ${CLIENT1} "su - $REAL_NAME -c './kill.sh &> log/kill.log'" &
done
	
sleep 1

echo Killing Team $NAME2
for REAL_NAME in ${TEAMALL2[@]}
do
	ssh ${CLIENT2} "su - $REAL_NAME -c './kill.sh &> log/kill.log'" &
done

countdown 6 ': if you want to stop league(CANCEL THIS GAME), press Ctrl+C.'

echo "Receiving game log file (wait a little...)"
rsync -avz --progress ${SERVER}:{rcssserver3d,sparkmonitor}.log .

GAMEDATE=`date +%Y%m%d%H%M`
GAME=`${RESULTPRINTER} sparkmonitor.log`
echo "Game information from log file: $GAME"

LOGNAME="${GAMEDATE}_${NAME1}_vs_${NAME2}"
GAME_ARCHIVE_DIR="gamelog/${NAME1}_vs_${NAME2}"
[ -d ${GAME_ARCHIVE_DIR} ] || mkdir -p ${GAME_ARCHIVE_DIR}

SCORE_LEFT=0
SCORE_RIGHT=0
if [ -n "${GAME}" ]; then
    echo $GAME >> ${RESULTFILE}
    set ${GAME}
    LOGNAME="${GAMEDATE}_$1_$3_vs_$2_$4"
    SCORE_LEFT=$3
    SCORE_RIGHT=$4
fi

GAME_TIME=`tail -n1 sparkmonitor.log | grep time | sed "s/.*time \([0-9]*\).*/\1/"`

LOGDIR="${GAME_ARCHIVE_DIR}/${LOGNAME}"
mkdir $LOGDIR
mv {sparkmonitor,rcssserver3d}.log $LOGDIR

# Check to see if the game was complete
if [ -z "$GAME_TIME" ]; then
    TOTAL_PLAYED=0
else
    TOTAL_PLAYED=$GAME_TIME
fi

if [ $TOTAL_PLAYED -lt $FULL_TIME ]; then
    echo $TOTAL_PLAYED > gametime
    echo "Game finished at time(rounded): $GAME_TIME"
    echo "Total played time is: $TOTAL_PLAYED"
    echo "Total game time is: $FULL_TIME"
    echo -n "Run complementary match? [y/N]:"
    read ans
    case $ans in
    [yY]*)
	countdown 7 ": wait complementary match."
	ssh ${SERVER} "sed -i 's/setTime(.*)/setTime($TOTAL_PLAYED)/' $SOCCERSIM_CONF"
	if [ ! $TOTAL_PLAYED -lt $HALF_TIME ] && [ ! -f scoresswitched ]; then
            ssh ${SERVER} "sed -i 's/setScores(.*)/setScores($SCORE_RIGHT,$SCORE_LEFT)/' $SOCCERSIM_CONF"
	else
            ssh ${SERVER} "sed -i 's/setScores(.*)/setScores($SCORE_LEFT,$SCORE_RIGHT)/' $SOCCERSIM_CONF"
	fi
	
	if [ "$TOTAL_PLAYED" -ne "300" -a "$TOTAL_PLAYED" -ne "600" -a  "$TOTAL_PLAYED" -ne "0" ]; then
		echo -n "Do you want restore the match? [y/N]:"
    	read restore
    	if [ $restore = "y" -o $restore = "Y" ]; then
    		sed -i 's/\RESTORE=false/\RESTORE=true/' ./var/conf
    		ssh ${SERVER} "cp sparkmonitor.log log_backup/"
    	fi
	fi 

	if [ $TOTAL_PLAYED -lt $HALF_TIME ] || [ -f scoresswitched ]; then
        ${CURDIR}/start $TEAM1 $TEAM2 $TIE_BREAK_SETTING ${TEAMALL1[@]} ${DELIMITER_PLAYERS} ${TEAMALL2[@]}
	else
	   touch scoresswitched
        ${CURDIR}/start $TEAM2 $TEAM1 $TIE_BREAK_SETTING ${TEAMALL2[@]} ${DELIMITER_PLAYERS} ${TEAMALL1[@]}
	fi
	exit 0
	;;
    esac
fi

TIE=""
if [ -z "${GAME}" ]; then
    echo -n "No clear winner(CANNOT READ SCORES), do you want to run a tie break game?[y/N]:"
    read ans
    case $ans in
    [yY]*)
    	TIE=y
    	;;
    esac
else
    set ${GAME}
    echo 
    [[ "x$3" = "x$4" && "$TIE_BREAK_GAME_ENABLED" = "true" ]] && TIE=y
fi

if [ "x$TIE" = 'xy' ]; then
    echo -n "Tie break, Run match again? [y/N]:"
    read ans
    case $ans in
    [yY]*)
	${CURDIR}/setup
	countdown 10 ": wait extend match."
        ${CURDIR}/start $TEAM1 $TEAM2 tiebreak ${TEAM_ALL1[@]} ${DELIMITER_PLAYERS} ${TEAM_ALL2[@]}
    ;;
    esac
fi

rm gametime
